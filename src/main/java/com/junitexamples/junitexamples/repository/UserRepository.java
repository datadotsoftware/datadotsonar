package com.junitexamples.junitexamples.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.junitexamples.junitexamples.entity.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findOneByUsername(String username);

}
